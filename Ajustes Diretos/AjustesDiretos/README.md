|   |   |
|---|---|
|Fonte | Portal BASE|
| Metodologia| Os dados foram compilados através dos contratos encontrados no Portal BASE, entre 2020 e 2022, nas pesquisas pelos termos "advogado", serviço jurídico", parecer jurídico" e "assessoria jurídica", das quais foram encontrados 2.551 contratos, que podem ser acedidos em [Original](Original).|
| Artigo(s)| [O império do ajuste direto](https://rr.sapo.pt/o-imperio-do-ajuste-direto)|
