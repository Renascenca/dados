|   |   |
|---|---|
|Fonte | Global Terrorism Database|
| Metodologia| Recorreu-se a [Global Terrorism Database](https://www.start.umd.edu/gtd/) e filtraram-se os dados por Europa Ociental |
| Artigo(s)| [O terrorismo mata hoje menos na Europa do que nos anos 70. Porque nos parece o contrário?](http://rr.sapo.pt/especial/84375/o_terrorismo_mata_hoje_menos_na_europa_do_que_nos_anos_70_porque_nos_parece_o_contrario)|