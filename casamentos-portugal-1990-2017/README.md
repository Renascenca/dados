|   |   |
|---|---|
|Fonte | INE|
| Metodologia| Os dados foram cedios pelo INE, e devidamente limpos e analisados pela RR|
| Análise | A análise foi feita em R, e pode ser consultada [aqui](https://gitlab.com/Renascenca/dados/blob/master/casamentos-portugal-1990-2017/analise.Rmd).Os dados disponibilizados pelo INE são apurados com base na informação registada nas Conservatórias de Registo Civil, decorrente dos dados constantes dos assentos de casamento e de informação adicional prestada pelos nubentes. Nos dados originais, nas situações em que não sejam apresentadas observações (nº de observações = 0) em determinada variável ou cruzamento de variáveis, a mesma aparece omissa no apuramento.Dados referentes a 2017, apurados com base em informação registada nas Conservatórias do Registo Civil até março de 2018.
| Artigo(s)| [Qual é o melhor dia para casar?]()|