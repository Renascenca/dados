Senhor Presidente
Senhoras e Senhores deputados

Este debate do Estado da Nação é necessariamente o debate de balanço desta Legislatura.

A Democracia vive das alternativas e a primeira marca desta Legislatura, foi a afirmação da vitalidade democrática contra o fatalismo, a inevitabilidade, a ideia que não havia outro caminho, que não havia alternativa.

Em Democracia há sempre alternativas, há sempre a possibilidade de abrir novos caminhos, há sempre a liberdade de escolher, há sempre a possibilidade de fazer diferente.

Como temos visto em tantos países, quando o sistema político não é capaz de gerar a mudança necessária, o impasse bloqueia as instituições, a descrença mina a confiança dos cidadãos e a Democracia cede ao populismo.

Quero por isso saudar os Grupos Parlamentares do PS, BE, PCP e PEV por terem ousado derrubar um muro anacrónico e assumido a responsabilidade de afirmar uma maioria parlamentar como alternativa de Governo, garantindo a mudança de política que os cidadãos desejavam e Portugal precisava.

Ao contrário do que muitos previram, a maioria que constituímos provou ser estável e duradoura no horizonte da Legislatura e capaz de cumprir integralmente as posições conjuntas que lhe deram consistência, de executar o Programa do Governo e de honrar os compromissos internacionais de Portugal.

A previsibilidade nas políticas foi, durante os últimos 4 anos, um dos fatores centrais de confiança.

Os portugueses deixaram de viver no sobressalto quotidiano dos cortes nas pensões ou nos salários, na incerteza do aumento de impostos ou do encerramento de serviços, na incógnita dos orçamentos retificativos, na instabilidade do permanente conflito constitucional.

Foram 4 anos a cumprir passo a passo os compromissos com os portugueses.

Foram 4 anos com 4 orçamentos, em cada um deles cumprindo o calendário das medidas previstas e alcançando as metas que estavam estimadas.

Foram 4 anos de sólida cooperação institucional do Governo com a Assembleia e o Presidente da República, escrupuloso respeito pela independência da Justiça, das Autonomias Regionais e do Poder Local.

Foram 4 anos em que nunca desistimos de um consenso parlamentar alargado para os investimentos estratégicos, como assegurámos nas recentes aprovações do Programa    Nacional de Investimentos 20/30 e das Leis de Programação Militar e de Infraestruturas Militares, fundamentais para apoiar o notável contributo das Forças Armadas, para a garantia da nossa soberania, o prestígio internacional de Portugal, e do serviço às populações.

Foram 4 anos em que eliminámos todas as discriminações na lei em função do género, da identidade e da orientação sexual.

Foram 4 anos de normalidade constitucional, sem um único pedido de fiscalização preventiva, e em que nenhuma norma do governo foi declarada inconstitucional pelo Tribunal Constitucional. Em suma, governámos sempre em harmonia com a Constituição.

A estabilidade política, a previsibilidade das políticas, a normalidade institucional, o respeito da Constituição são elementos fundamentais para o grande ganho desta Legislatura: a recuperação da Confiança.

Os dados do Eurobarómetro são concludentes: esta Legislatura foi determinante para a recuperação da confiança dos portugueses no funcionamento da Democracia, que mais do que duplicou, de 28% em 2015 para 64% no final de 2018.

Senhor Presidente,
Senhoras e Senhores Deputados.

No debate de discussão do Programa do Governo assumi um triplo desígnio para a Legislatura: Mais Crescimento, Melhor Emprego, Maior Igualdade.

Quatro anos volvidos, que balanço podemos fazer deste triplo desígnio?

Primeiro, mais crescimento. Portugal cresce 9% em termos reais nestes 4 anos, tendo retomado em 2017 e prosseguido em 2018 e 2019 um crescimento superior à média da UE, retomando a convergência interrompida no início deste século.

É um crescimento fortemente sustentado no investimento empresarial – apoiado na elevada execução do PT2020 - e no aumento das exportações, o que reforça a confiança do crescimento poder prosseguir com os ganhos de produtividade que o investimento induz e de competitividade que maiores quotas de mercado demonstram.

Segundo, mais e melhor emprego. Nos quatro anos desta legislatura foram criados 350.000 novos postos de trabalho, em simultâneo com um aumento do rendimento médio mensal líquido dos trabalhadores de 8,2% e uma subida do salário mínimo de quase 20%.

Mas a melhor evidência da maior qualidade do emprego é o facto de 89% dos novos empregos por conta de outrem, serem contratos sem termo, o que confirma que a confiança no futuro da economia é o melhor antídoto contra a precariedade.

Terceiro, maior igualdade. De 2015 para 2017, houve 180 mil famílias que saíram da situação de risco de pobreza, e 382 mil famílias que se libertaram da situação de privação material severa e, o rácio que compara o rendimento dos 10% mais ricos com o dos 10% mais pobres, reduziu-se para o valor mais baixo de sempre nas desigualdades.

Em síntese, mais crescimento, melhor emprego, maior igualdade, provam que há mesmo “mais vida para além do Orçamento”!

Não foi a destruição de direitos que gerou crescimento; não foi o corte dos salários que criou emprego; não foi o enorme aumento de impostos que promoveu a igualdade; não foi a austeridade que nos permitiu ter contas certas.

Pelo contrário, foi o virar da página da austeridade quer permitiu a recuperação de rendimentos; a recuperação de rendimentos que gerou confiança; a confiança que    motivou o investimento; o investimento que criou emprego; o emprego que garantiu maior rendimento.

Este é o círculo virtuoso, que abriu o caminho sustentável para termos contas certas, com o défice mais baixo da democracia e a dívida pública a recuar em 2018 para 121,5% e este ano a continuar a reduzir para 119% do PIB.

Sejamos claros. Nem o Diabo apareceu, nem a austeridade se disfarçou.

Estes são os resultados que alcançámos, ao mesmo tempo que o investimento público financiado pelo OE aumentou 45%, que criámos a Prestação Social para a Inclusão que abrange mais de 93 mil pessoas, que revertemos os cortes e aumentámos o valor real de mais de 3 milhões de pensões, que aliviámos os portugueses de 1.000 M€ de IRS/ano, que reduzimos as taxas moderadoras em 15%, que disponibilizámos manuais escolares gratuitos a todos os alunos do ensino básico e secundário, que aumentámos o abono de família de 300.000 crianças, e que reduzimos drasticamente o custo dos transportes públicos.

Mais crescimento, mais e melhor emprego e maior igualdade, com contas certas. É essa a chave da credibilidade que o país conquistou, e que é outro fator essencial para a confiança em Portugal.

A confiança que nos permitiu sair do Procedimento por Défice Excessivo e das notações de lixo; a confiança que sustenta a contínua redução das taxas de juro de 4% para 0,5%, permitindo-nos pagar este ano menos 2.000 M€ de juros do que pagávamos em 2014.

A confiança que atraiu valores record de investimento direto estrangeiro e estancou a emigração massiva, garantindo de novo, nos últimos dois anos, saldos migratórios positivos.

Em suma, nestes quatro anos Portugal melhorou, melhorando a vida de todos os portugueses.

Senhor Presidente,
Senhoras e Senhores Deputados,

Cumprimos o que prometemos e temos resultados.

Mas sabemos que a confiança só é sustentável quando se projeta no futuro, inspirada numa visão de médio e longo prazo, como a da Agenda para a Década que apresentámos em 2014 e que começámos a construir nesta Legislatura.

Com o reforço da coesão com a nossa diáspora, alargando a concessão de nacionalidad e aprofundando os direitos de participação na vida nacional; com um Estado mais ágil e próximo, que o Simplex e a Descentralização configuram; com a prioridade à efetiva igualdade de género e à conciliação da vida pessoal, familiar e profissional; com a valorização do interior, a reforma da floresta, o programa do regadio; com uma nova geração de políticas de habitação; com a prioridade à economia azul e a definição de uma estratégia para o espaço; com o incentivo ao empreendedorismo e à economia digital; o apoio à capitalização das empresas e o reforço da sua autonomia financeira; a renovada prioridade à educação de adultos e formação ao longo da vida; ao investimento na Cultura, Ciência e Educação como as bases da sociedade do conhecimento; com um roteiro para a neutralidade carbónica até 2050.

Esta não foi só uma Legislatura a reparar o passado e a cuidar do presente. Foi também uma Legislatura que lançou as bases do futuro, assente num modelo de desenvolvimento em que a inovação é o motor do crescimento sustentável.

É porque nestes 4 anos estivemos também focados no futuro que demos prioridade a grandes desafios estratégicos como: o desafio demográfico, as alterações climáticas, o desafio da sociedade digital ou a sustentabilidade da Segurança Social.

E também nesta dimensão estratégica temos resultados:

⎯ A UNICEF considerou que Portugal tem, a par da Suécia, a Noruega, a Islândia e a Estónia, uma das melhores políticas de apoio à família entre 31 países desenvolvidos.

⎯ O painel da UE sobre inovação registou os progressos de Portugal, colocando-nos como líder nas PME’s inovadoras e a décimas de sermos reclassificados como um país  fortemente inovador.

⎯ Segundo a Comissão Europeia, Portugal foi o país da UE que no ano passado mais reduziu as emissões de CO2, uma redução que foi mesmo a tripla da média da UE.

⎯ E, posso hoje anunciar, que, encerradas as contas de 2018, já alargámos a sustentabilidade da Segurança Social em mais 22 anos.

Senhor Presidente,
Senhoras e Senhores Deputados,

Não quero ser mal entendido. Não vivemos no oásis, num país cor de rosa. O balanço positivo destes 4 anos não nos permite esquecer os problemas que subsistem. As medidas adotadas, não prescindem das medidas que temos de adotar. O caminho já percorrido, não dispensa o caminho que ainda temos por percorrer. O já alcançado só reforça a nossa motivação e determinação para fazer o que falta fazer.

Portugal subiu de 18.º para 3.º país mais seguro do mundo, mas precisamos de continuar a investir nas nossas forças e serviços de segurança e nas condições de trabalho dos que nelas servem os portugueses.

Nestes 4 anos reduzimos em 35% as pendências processuais, mas temos de continuar a trabalhar para uma maior celeridade da justiça.

Temos hoje a mais baixa taxa de abandono escolar precoce de sempre, mas temos de continuar a investir na qualidade da escola pública.

O número de estudantes no ensino superior aumentou 4%, mas temos de continuar a melhorar a ação social escolar para universalizar o acesso a formações superiores.

Admitimos mais 11 mil profissionais no SNS, mas temos que continuar a reduzir os tempos de espera em saúde.

Crescemos mais, mas mais precisamos de crescer; há melhor emprego, mas o emprego tem de continuar a melhorar; há maior igualdade, mas as desigualdades têm de continuar a reduzir.

Conhecemos as necessidades do país e a legítima exigência dos portugueses. Por isso, não negamos os problemas. E para cada problema procuramos a melhor solução. Permitam-me 3 exemplos:

1º - Perante as dificuldades de renovação do cartão de cidadão, simplificámos o processo de renovação, aumentámos os locais em que se pode renovar o cartão, estamos a contactar preventivamente os cidadãos que vão precisar de renovar propondo agendamento programado e implementámos a possibilidade de fazer a renovação online do cartão. Com o conjunto destas medidas, de maio para junho aumentámos o número de cartões emitidos de 250 mil para 305 mil.

2º - Depois de anos de desinvestimento, eram manifestas as dificuldades na capacidade de resposta dos transportes públicos; por isso agimos no longo prazo com um forte investimento na aquisição de novos sistemas de segurança e sinalização, de composições para a CP, Metros de Lisboa e Porto e navios para a Transtejo.

Nesta Legislatura investimos quatro vezes mais que na legislatura anterior em transportes públicos.

Mas temos simultaneamente procurado responder no curto e médio prazo, na manutenção e recuperação de equipamento imobilizado: hoje a Soflusa e a Transtejo já dispõem de navios de reserva, o Metro de Lisboa já tem em circulação as 30 composições que estavam imobilizadas; e o Plano de Recuperação de Material Ferroviário que aprovámos permitirá também repor em circulação dezenas de composições, sendo que a conclusão este mês das obras de eletrificação da linha do Douro até Marco de Canavezes e da linha do Minho até Viana do Castelopermitirá deslocar as composições que aí hoje circulam para reforçar outras linhas, designadamente a linha do Oeste.

3º - Também na saúde procuramos responder aos problemas históricos que se arrastaram anos, ou aos problemas pontuais ou sazonais que vão ocorrendo, como fizemos ainda na semana passada com a gestão do plano de férias das maternidades de Lisboa. Seguramente o exemplo mais impressivo é o da ala pediátrica do Hospital de S. João. Depois de mais de uma década sem solução para o problema da Ala Pediátrica, elaborámos e aprovámos o novo projeto para o edifício, garantimos o financiamento necessário e estamos em condições de adjudicar a obra nas próximas semanas, mas, desde já, reinstalámos as crianças no edifício principal.

Não negamos, nem fugimos dos problemas. Não nos resignamos, e procuramos sempre a solução, mesmo quando esta é difícil ou morosa. A nós compete-nos fazer e é isso mesmo que fazemos!

Senhor Presidente,
Senhores Deputados,

O momento é de balanço e estamos aqui para prestar contas aos portugueses. Do tanto que foi feito em apenas quatro anos e do tanto que ainda queremos fazer.

A história fará a sua justiça a seu tempo, com a moderação e o distanciamento necessários. Mas hoje, atrevo-me a dizer, conseguimos!

Tínhamos mesmo outro caminho, era mesmo possível fazer diferente, havia mesmo uma   alternativa, podíamos mesmo escolher. Contra o fatalismo, a inevitabilidade, o pessimismo, provámos que sim, era possível.

Quero agradecer a todos os que nesta Assembleia construíram esta alternativa, quero agradecer a todas e a todos os que serviram como meus colegas de Governo, mas quero sobretudo agradecer às portuguesas e aos portugueses.

Quatro anos volvidos, o país recuperou a dignidade, a autoestima, o respeito internacional e encara o futuro com otimismo.

Portugal está melhor do que há quatro anos porque os Portugueses vivem melhor do que há quatro anos.

Portugal está melhor porque, os Portugueses recuperaram a confiança, recuperaram a esperança no seu futuro, no futuro dos seus, no futuro do País.

A esperança assente nas bases sólidas do que temos vindo a construir nestes quatro anos e na certeza de que é possível continuar a fazer mais e melhor.

Como Primeiro-Ministro partilho a tranquilidade da consciência com o desassossego da vontade.

Mas sou, principalmente, um cidadão português muito orgulhoso no meu País.
Muito obrigado.
