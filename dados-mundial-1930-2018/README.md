|   |   |
|---|---|
|Fonte | FIFA|
| Metodologia| Recorrendo a um scraper em bash, a **Renascença** recolheu os dados do site da FIFA de [todos so resultados em cada ano](data-scraped). Depois de limpeza em R, o ficheiro [wc_matches](wc_matches.csv) tem todos os resultados de todos os jogos entre 1930 e 2014 num campeonato no mundo.|
| Artigo(s)| [Interactivo. Consulte as vagas e médias de acesso ao ensino superior](http://rr.sapo.pt/el/115222/acha-que-sabe-tudo-sobre-o-mundial)|